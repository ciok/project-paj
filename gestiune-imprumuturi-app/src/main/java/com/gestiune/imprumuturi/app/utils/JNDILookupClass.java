package com.gestiune.imprumuturi.app.utils;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class JNDILookupClass {
	
	private static Context initialContext;
	
	public static Context getInitialContext() throws NamingException {
		if (initialContext == null) {
			Properties jndiProperties = new Properties();
			jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
			jndiProperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");
			jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
			jndiProperties.put("jboss.naming.client.ejb.context", true);
			
			initialContext = new InitialContext(jndiProperties);
		}
		
		return initialContext;
	}
	
	public static String getLookupName(final Class<?> beanClass) {
		// Deployed EAR name
		String appName = "gestiune-imprumuturi-ear";
		
		// Deployed JAR name
		String moduleName = "gestiune-imprumuturi-ejb";
		
		// The EJB bean implementation class name
		String beanName = beanClass.getSimpleName();
		
		// Fully qualified remote interface name
		String interfaceName = beanClass.getInterfaces()[0].getName();
		
		//Create look up string name
		String lookupName = "java:" + appName + "/" + moduleName + "/" + beanName + "!" + interfaceName;
		
		return lookupName;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(Class<T> beanClass) {
		Context context = null;
		
		try {
			// Obtain Context
			context = JNDILookupClass.getInitialContext();
			
			if (context == null) {
				System.out.println("context is null");
			}
			
			// Generate JNDI Lookup name
			String lookupName = JNDILookupClass.getLookupName(beanClass);
			
			System.out.println(lookupName);
			
			// Lookup and cast
			return (T) context.lookup(lookupName);
		} catch (final NamingException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
