package com.gestiune.imprumuturi.app;

import java.util.List;

import com.gestiune.imprumuturi.app.utils.BeanFactory;
import com.gestiune.imprumuturi.ejb.studenti.IStudenti;
import com.gestiune.imprumuturi.entities.Student;

public class Client {

	public static void main(String... args) {
		IStudenti bean = BeanFactory.getStudentiBean();
		
		Student s1 = new Student();
		s1.setCnp("1234");
		s1.setNume("Ciouca");
		s1.setPrenume("Alexandru");
		
		Student s2 = new Student();
		s2.setCnp("5678");
		s2.setNume("Gigi");
		s2.setPrenume("Pateu");
		
		// salvez studenti noi
		//bean.saveStudent(s1);
		//bean.saveStudent(s2);
		
		Student s3 = bean.findStudent(s2);
		System.out.println(s3);
		System.out.println();
		
		System.out.println("List:");
		List<Student> s4 = bean.getAllStudents();
		
		for (Student student : s4) {
			System.out.println(student);
		}
	}
}
