package com.gestiune.imprumuturi.app.utils;

import com.gestiune.imprumuturi.ejb.studenti.IStudenti;
import com.gestiune.imprumuturi.ejb.studenti.StudentiBean;

public class BeanFactory {

	public static IStudenti getStudentiBean() {
		return JNDILookupClass.getBean(StudentiBean.class);
	}
}
