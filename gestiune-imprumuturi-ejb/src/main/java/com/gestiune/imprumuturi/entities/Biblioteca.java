package com.gestiune.imprumuturi.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the biblioteci database table.
 * 
 */
@Entity(name = "biblioteci")
@NamedQuery(name = "biblioteci.findAll", query = "SELECT b FROM biblioteci b")
public class Biblioteca implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cod_biblioteca")
	private Long codBiblioteca;

	private String denumire;

	@Column(name = "adresa_b")
	private String adresa;

	private String site;

	@Column(name = "data_infiintarii")
	private Date dataInfiintarii;

	public Biblioteca() {
	}

	public Long getCodBiblioteca() {
		return codBiblioteca;
	}

	public void setCodBiblioteca(Long codBiblioteca) {
		this.codBiblioteca = codBiblioteca;
	}

	public String getDenumire() {
		return denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public Date getDataInfiintarii() {
		return dataInfiintarii;
	}

	public void setDataInfiintarii(Date dataInfiintarii) {
		this.dataInfiintarii = dataInfiintarii;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String toString() {
		return "Biblioteca [cod = " + codBiblioteca + ", denumire = " + denumire + "]";
	}
}
