package com.gestiune.imprumuturi.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the studenti database table.
 * 
 */
@Entity(name = "studenti")
@NamedQuery(name = "studenti.findAll", query = "SELECT s FROM studenti s")
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String cnp;

	private String adresa;

	private String email;

	private String nume;

	private String prenume;

	private String telefon;

	public Student() {
	}

	public String getCnp() {
		return this.cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getAdresa() {
		return this.adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return this.prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getTelefon() {
		return this.telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	public String toString() {
		return "Student [cnp = " + cnp + ", nume=" + nume + ", prenume=" + prenume + "]";
	}
}
