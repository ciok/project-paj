package com.gestiune.imprumuturi.ejb.gestiuni;

import java.util.List;

import javax.ejb.Remote;

import com.gestiune.imprumuturi.entities.Student;

@Remote
public interface IGestiuni {

	void saveStudent(Student student);

	Student findStudent(Student student);

	List<Student> getAllStudents();
}
