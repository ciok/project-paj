package com.gestiune.imprumuturi.ejb.studenti;

import java.util.List;

import javax.ejb.Remote;

import com.gestiune.imprumuturi.entities.Student;

@Remote
public interface IStudenti {

	void saveStudent(Student student);

	Student findStudent(Student student);

	List<Student> getAllStudents();
}
