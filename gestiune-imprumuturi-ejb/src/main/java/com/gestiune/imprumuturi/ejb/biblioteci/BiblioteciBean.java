package com.gestiune.imprumuturi.ejb.biblioteci;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.gestiune.imprumuturi.entities.Student;

/**
 * Session Bean implementation class StudentiBean
 */
@Stateless
public class BiblioteciBean implements IBiblioteci {

	@PersistenceContext(unitName = "PAJ2EE")
	private EntityManager entityManager;
	
    /**
     * Default constructor. 
     */
    public BiblioteciBean() {
    }

	@Override
	public void saveStudent(Student student) {
		entityManager.persist(student);
	}

	@Override
	public Student findStudent(Student student) {
		Student s = entityManager.find(Student.class, student.getCnp());
		
		return s;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Student> getAllStudents() {
		Query query = entityManager.createNamedQuery("studenti.findAll");
		List<Student> studenti = query.getResultList();
		
		return studenti;
	}
}
